# -*- coding: utf-8 -*-
# :Project:   metapensiero.sphinx.autodoc_sa — Development flake
# :Created:   gio 23 giu 2022, 15:33:09
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2022, 2023 Lele Gaifax
#

{
  description = "metapensiero.sphinx.autodoc_sa development shell";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      # Use the same nixpkgs
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, gitignore }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        inherit (builtins) fromTOML getAttr listToAttrs map readFile replaceStrings;
        pkgs = import nixpkgs { inherit system; };
        inherit (pkgs.lib) cartesianProductOfSets genAttrs;
        inherit (gitignore.lib) gitignoreFilterWith;

        getSource = name: path: pkgs.lib.cleanSourceWith {
          name = name;
          src = path;
          filter = gitignoreFilterWith { basePath = path; };
        };

        # Python versions to test against: "python3" is the current major version
        # in NixOS
        pyVersions = [
          "python3"
          # "python39"
          # "python310"
          # "python311"
        ];

        # SQLAlchemy versions to try out
        saVersions = [
          { version = "1.4.47";
            sha256 = "95fc02f7fc1f3199aaa47a8a757437134cf618e9d994c84effd53f530c38586f"; }
          { version = "2.0.8";
            sha256 = "250e158a1f36c965dde1f949366eae9a57504a8cd7a4d968e66c2d0b3c18198d"; }];

        mkSAPkg = python: saVersion:
          python.pkgs.buildPythonPackage rec {
            pname = "SQLAlchemy";
            version = saVersion.version;
            src = python.pkgs.fetchPypi {
              inherit pname version;
              sha256 = saVersion.sha256;
            };
            doCheck = false;
            nativeBuildInputs = [ python.pkgs.cython ];
            propagatedBuildInputs = [
              python.pkgs.greenlet
              python.pkgs.typing-extensions
            ];
          };

        mkPkg = pyVersion: saVersion: doCheck:
          let
            py = getAttr pyVersion pkgs;
            sqlalchemy' = mkSAPkg py saVersion;
            pinfo = (fromTOML (readFile ./pyproject.toml)).project;
          in py.pkgs.buildPythonPackage {
            inherit doCheck;
            pname = "${pinfo.name}-${saVersion.version}";
            version = pinfo.version;
            format = "pyproject";
            src = getSource "autodoc_sa" ./.;
            checkPhase = "pytest";
            checkInputs = with py.pkgs; [
              pglast
              pytest
              sphinx
              sqlalchemy'
            ];
            nativeBuildInputs = with py.pkgs; [
              pdm-pep517
            ];
            propagatedBuildInputs = with py.pkgs; [
              progressbar2
              ruamel-yaml
              sphinx
              sqlalchemy'
            ];
          };

        checkPkgs = map
          (combo: mkPkg combo.pyv combo.sav true)
          (cartesianProductOfSets { pyv = pyVersions; sav = saVersions; });

        mkTestShell = pyVersion: saVersion:
          let
            py = getAttr pyVersion pkgs;
            pkg = mkPkg pyVersion saVersion false;
            env = py.buildEnv.override {
              extraLibs = [ pkg ];
            };
          in pkgs.mkShell {
            name = "py-${py.version}+sa-${saVersion.version}";
            packages = with pkgs; [
              just
              env
            ];
          };

        testShells = map
          (combo: mkTestShell combo.pyv combo.sav)
          (cartesianProductOfSets { pyv = pyVersions; sav = saVersions; });
      in {
        devShells = {
          default = pkgs.mkShell {
            name = "Dev shell for mp.sphinx.autodoc_sa";

            packages = (with pkgs; [
              bump2version
              just
              python3
              twine
            ]) ++ (with pkgs.python3Packages; [
              build
              pglast
              pytest
              sphinx
              sqlalchemy
              tomli
            ]);

            shellHook = ''
               export PYTHONPATH="$(pwd)/src''${PYTHONPATH:+:}$PYTHONPATH"
             '';
          };
        } // (listToAttrs (map (s: {
          name = replaceStrings ["."] ["_"] s.name;
          value = s;
        }) testShells));

        checks = listToAttrs (map (p: {
          name = replaceStrings ["."] ["_"] p.name;
          value = p;
        }) checkPkgs);
      });
}
